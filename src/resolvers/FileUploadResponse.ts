import { Field, ObjectType } from "type-graphql";

@ObjectType()
export class FileUploadResponse {
  @Field(() => String)
  url: string;
}
