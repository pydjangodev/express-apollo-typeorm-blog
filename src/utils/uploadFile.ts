import path from "path";
import { createWriteStream, ReadStream } from "fs";
import { createRandomFilename } from "./createRandomFilename";
import { finished } from "stream/promises";

export const uploadFile = async (
  stream: ReadStream,
  filename: string,
  pos: string
): Promise<{ uploadedFileName: string; serviceUrl: string }> => {
  const { ext } = path.parse(filename);
  const randomFilename = createRandomFilename() + ext;
  const filePath = path.join(__dirname, `../../images/${pos}`, randomFilename);
  const out = createWriteStream(filePath);
  await stream.pipe(out);
  await finished(out);
  return {
    uploadedFileName: randomFilename,
    serviceUrl: `http://localhost:4000/images/${pos}/${randomFilename}`,
  };
};
